package com.dxc.service;

import com.dxc.entity.Post;
import com.dxc.payload.PostDTO;
import com.dxc.payload.PostResponse;

public interface PostServiceInterface {

	PostDTO createPost(PostDTO postDTO);

	PostResponse getAllPosts(int pageNo, int pageSize, String sortBy, String sortDir);

	PostDTO getPostById(Long id);

	PostDTO updatePost(PostDTO postDTO, Long id);

	void deletePostById(long id);

}
