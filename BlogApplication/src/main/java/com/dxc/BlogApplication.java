package com.dxc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogApplication.class, args);
	}

}

/*
Methods for postman testing

POST http://localhost:8080/api/posts

GET    http://localhost:8080/api/posts?pageNo=1&pageSize=3&sortBy=id

GETBYID http://localhost:8080/api/posts/1

PUT http://localhost:8080/api/posts/1

DELETE http://localhost:8080/api/posts/1

*/
