package com.dxc.service.impl;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.dxc.entity.Post;
import com.dxc.exception.ResourceNotFoundException;
import com.dxc.payload.PostDTO;
import com.dxc.payload.PostResponse;
import com.dxc.repository.PostRepository;
import com.dxc.service.PostServiceInterface;


@Service
public class PostServiceImplementation implements PostServiceInterface {

	@Autowired
	private PostRepository postRepo;
	
	// Convert entity to DTO
	private PostDTO mapToDTO(Post post) {
		PostDTO postDTO = new PostDTO();
		postDTO.setId(post.getId());
		postDTO.setTitle(post.getTitle());
		postDTO.setDescription(post.getDescription());
		postDTO.setContent(post.getContent());
		
		return postDTO;
	}
	// Convert DTO to entity
	public Post mapToEntity(PostDTO postDTO) {
		Post post = new Post();
		post.setTitle(postDTO.getTitle());
		post.setDescription(postDTO.getDescription());
		post.setContent(postDTO.getContent());
		
		return post;
	}
	
	// Create Post
	@Override
	public PostDTO createPost(PostDTO postDTO) {
		// Convert DTO to entity
		Post post = mapToEntity(postDTO);
		Post newPost = postRepo.save(post);
		
		// Convert entity to DTO
		PostDTO postResponse = mapToDTO(newPost);
		
		return postResponse;
	}
	
	@Override
	public PostResponse getAllPosts(int pageNo, int pageSize, String sortBy, String sortDir) {
		Sort sort= sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ?
				Sort.by(sortBy).ascending()
				: Sort.by(sortBy).descending();
		
		Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
		Page<Post> posts = postRepo.findAll(pageable);
		
		// Get content from page object
		List<Post> listOfPosts = posts.getContent();
		List<PostDTO> content = (List<PostDTO>) listOfPosts.stream().map(
				post -> mapToDTO(post)).collect(Collectors.toList()
				);
		PostResponse postResponse = new PostResponse();
		
		postResponse.setContent(content);
		postResponse.setPageNo(posts.getNumber());
		postResponse.setPageSize(posts.getSize());
		
		
		return null;
	}
	
	@Override
	public PostDTO getPostById(Long id) {
		Post post=postRepo.findById(id).orElseThrow(()->new ResourceNotFoundException("POST", "id", id));
		return mapToDTO(post);
	}
	
	@Override
	public PostDTO updatePost(PostDTO postDTO, Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void deletePostById(long id) {
		// TODO Auto-generated method stub
		
	}
}
