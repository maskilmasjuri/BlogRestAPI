package com.dxc.service.impl;

import org.springframework.stereotype.Service;

import com.dxc.entity.Post;
import com.dxc.payload.PostDTO;
import com.dxc.repository.PostRepository;
import com.dxc.service.PostServiceInterface;

@Service
public class PostServiceImplementation implements PostServiceInterface {

	private PostRepository postRepo;
	
	// Convert entity to DTO
	private PostDTO mapToDTO(Post post) {
		PostDTO postDTO = new PostDTO();
		postDTO.setId(post.getId());
		postDTO.setTitle(post.getTitle());
		postDTO.setDescription(post.getDescription());
		postDTO.setContent(post.getContent());
		
		return postDTO;
	}
	// Convert DTO to entity
	public Post mapToEntity(PostDTO postDTO) {
		Post post = new Post();
		post.setTitle(postDTO.getTitle());
		post.setDescription(postDTO.getDescription());
		post.setContent(postDTO.getContent());
		
		return post;
	}
	
	// Create Post
	@Override
	public PostDTO createPost(PostDTO postDTO) {
		// Convert DTO to entity
		Post post = mapToEntity(postDTO);
		Post newPost = postRepo.save(post);
		
		// Convert entity to DTO
		PostDTO postResponse = mapToDTO(newPost);
		
		return postResponse;
	}
}
